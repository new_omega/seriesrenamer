﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;

namespace SeriesRenamer
{
    class Program
    {
        static string _n = Environment.NewLine;

        static void Main(string[] args)
        {
            string input;
            do
            {
                Console.WriteLine("Введите путь к папке с файлами:");
                input = Console.ReadLine();
                DirectoryInfo di = new DirectoryInfo(input);
                Console.Write("Введите маску для поиска файлов: ");
                input = Console.ReadLine();
                // Добавить проверку на валидность маски
                string mask = input;
                Console.WriteLine("Найдены следующие файлы:");
                foreach (FileInfo fi in di.GetFiles(input).OrderBy(ks => ks.Name.Trim()))
                {
                    Console.WriteLine(fi.Name);
                }

                int count = di.GetFiles(input).Length;
                Console.WriteLine($"Всего: {count}");
                Console.WriteLine("Все серии будут переименованы, с укзаанием порядкого номера в конце названия. С какого числа начать нумерацию?");
                Console.Write("Введите число (по умолчанию - 1): ");
                input = Console.ReadLine();
                int startIndex = 1;
                if (input != "")
                    startIndex = Convert.ToInt32(input);
                Console.WriteLine($"Введите новое название. К нему будет добавлен порядковый номер, начиная с {startIndex}.{_n}" +
                    $"Номер будет дополнен нулями так, чтобы название каждого файла имело одинаковую длину.{_n}Новое название:");
                string newTitle = Console.ReadLine();
                Console.WriteLine($"Хотите добавить постфикс к файлу? Например: \"ENG\", \"JP\", \"рус\".{_n}" +
                    "Если да, то введите постфикс. В противном случае, не вводите ничего и нажмите Enter.");
                string postfix = Console.ReadLine();
                if (postfix != "")
                    postfix = "." + postfix;
                int index = startIndex;
                Console.WriteLine("Новые названия будут выглядеть так:");
                foreach (FileInfo fi in di.GetFiles(mask).OrderBy(ks => ks.Name.Trim()))
                {
                    string seriesTitle = GetTitle(newTitle, postfix, index, count);
                    index++;

                    Console.WriteLine($"{seriesTitle}{fi.Extension}");
                }
                Console.WriteLine("Если всё ок, нажмите Enter. Будет выполнено переименование.");
                Console.ReadLine();
                index = startIndex;
                foreach (FileInfo fi in di.GetFiles(mask).OrderBy(ks => ks.Name.Trim()))
                {
                    string seriesTitle = GetTitle(newTitle, postfix, index, count);
                    index++;

                    fi.MoveTo($"{di.FullName}\\{seriesTitle}{fi.Extension}");
                }
                Console.WriteLine("Готово. Если хотите повторить операцию, введите Y. Для выхода, нажмите Enter.");
                input = Console.ReadLine();
            } while (input.ToLower().Equals("y"));
        }

        private static string GetTitle(string title, string postfix, int index, int count)
        {
            string result = title + " " + index.ToString() + postfix;
            if (count > 9 && index < 10)
                result = title + " 0" + index.ToString() + postfix;

            return result;
        }
    }
}
